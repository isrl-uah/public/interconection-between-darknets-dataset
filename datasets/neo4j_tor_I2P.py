#this script loads the dataset into a neo4j database

from neo4j import GraphDatabase

neoHost = "localhost"
neoPort = "7687"

neoUser = "databaseUser"
neoPassword = "" #set user and passwd or disable in DB settings (dbms.security.auth_enabled=false)

driver = GraphDatabase.driver("bolt://" +  neoHost + ":" + neoPort, auth=(neoUser, neoPassword))
fp = open('./i2p_results.csv', 'r') #set file name


def add_domain(tx, name, domainNetwork):
    tx.run("MERGE (d:Domain {name: $name, network: $network})",
           name=name, network=domainNetwork)

def add_child(tx, name, domainNetwork, parent, parentNetwork):
    tx.run("MATCH (d:Domain {name: $name, network: $network}) MERGE (p:Domain {name: $parent, network: $parentNetwork}"
                ") CREATE UNIQUE (p)<-[:CHILD {}]-(d) ",
                name=name, network=domainNetwork, parent=parent, parentNetwork=parentNetwork)


with driver.session() as session:
    for line in fp:
        line = line.split(",")
        domain = line[0].strip()
        domainNetwork = line[1].strip()
        parent = line[2].strip()
        parentNetwork = line[3].strip()
        session.write_transaction(add_domain, domain, domainNetwork)
        session.write_transaction(add_child, domain, domainNetwork, parent, parentNetwork)






